package edu.tucn.secourseexamples.lesson4.ex1association;

/**
 * @author Radu Miron
 * @version 1
 */
public class BlogEntry {
    private String content;

    public BlogEntry(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "BlogEntry{" +
                "content='" + content + '\'' +
                '}';
    }
}
