package edu.tucn.secourseexamples.lesson4.ex1association;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        BlogAccount blogAccount = new BlogAccount();

        while (true) {
            System.out.println("\n 1. Add new entry; 2. Print all entries \n");
            int choice = scanner.nextInt();

            switch (choice) {
                case 1:
                    System.out.println("\n Please enter the content: \n");
                    String content = scanner.next();
                    blogAccount.add(new BlogEntry(content));
                    break;

                case 2:
                    blogAccount.printAllBlogEntries();
                    break;

                default:
                    System.err.println("\nInvalid choice!\n");
            }
        }
    }
}
