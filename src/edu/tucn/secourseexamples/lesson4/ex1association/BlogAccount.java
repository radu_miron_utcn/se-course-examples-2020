package edu.tucn.secourseexamples.lesson4.ex1association;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class BlogAccount {
    private BlogEntry[] blogEntries;
    private int counter;

    public BlogAccount() {
        this.blogEntries = new BlogEntry[100];
    }

    public void add(BlogEntry blogEntry) {
        this.blogEntries[counter++] = blogEntry;
    }

    public void printAllBlogEntries() {
        Arrays.stream(blogEntries)
                .filter(e -> e != null)
                .forEach(e -> System.out.println(e + "\n\n"));
    }
}
