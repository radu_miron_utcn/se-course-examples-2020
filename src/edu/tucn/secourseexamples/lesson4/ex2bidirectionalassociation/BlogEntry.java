package edu.tucn.secourseexamples.lesson4.ex2bidirectionalassociation;

/**
 * @author Radu Miron
 * @version 1
 */
public class BlogEntry {
    private BlogAccount blogAccount;
    private String content;

    public BlogEntry(String content, BlogAccount blogAccount) {
        this.content = content;
        this.blogAccount = blogAccount;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "BlogEntry{" +
                "content='" + content + "\', " +
                "accountName='" + blogAccount.getName() + '\'' +
                '}';
    }
}
