package edu.tucn.secourseexamples.lesson4.ex2bidirectionalassociation;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class BlogAccount {
    private String name;
    private BlogEntry[] blogEntries;
    private int counter;

    public BlogAccount(String name) {
        this.blogEntries = new BlogEntry[100];
        this.name = name;
    }

    public void add(BlogEntry blogEntry) {
        this.blogEntries[counter++] = blogEntry;
    }

    public void printAllBlogEntries() {
        Arrays.stream(blogEntries)
                .filter(e -> e != null)
                .forEach(e -> System.out.println(e + "\n\n"));
    }

    public String getName() {
        return name;
    }
}
