package edu.tucn.secourseexamples.lesson12.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public class ConsoleMessageWriter extends MessageWriter {
    @Override
    public void writeMessage(String message) {
        System.out.println(message);
    }
}
