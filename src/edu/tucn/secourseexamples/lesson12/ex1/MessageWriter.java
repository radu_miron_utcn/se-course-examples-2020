package edu.tucn.secourseexamples.lesson12.ex1;

/**
 * @author Radu Miron
 * @version 1
 */
public abstract class MessageWriter {
    public abstract void writeMessage(String message);
}
