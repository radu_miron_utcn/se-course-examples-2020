package edu.tucn.secourseexamples.lesson12.ex1;

import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileMessageWriter extends MessageWriter {
    @Override
    public void writeMessage(String message) {
        try (FileWriter fw = new FileWriter("/tmp/message.txt", true)) {
            fw.write(message + "\n");
            fw.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
