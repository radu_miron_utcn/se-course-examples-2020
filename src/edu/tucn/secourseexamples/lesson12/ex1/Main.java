package edu.tucn.secourseexamples.lesson12.ex1;

import java.util.Scanner;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        MessageWriter messageWriter;

        System.out.println("Please choose: \n1. Console writer \n2.File writer");
        Scanner scanner = new Scanner(System.in);
        String choice = scanner.nextLine();

        switch (choice) {
            case "1":
                messageWriter = new ConsoleMessageWriter();
                break;
            case "2":
                messageWriter = new FileMessageWriter();
                break;
            default:
                throw new IllegalArgumentException("Bad choice");
        }

        String message = null;

        while (message == null || !message.equals("stop")) {
            System.out.println("Input you message:");
            message = scanner.nextLine();
            messageWriter.writeMessage(message);
        }

    }
}
