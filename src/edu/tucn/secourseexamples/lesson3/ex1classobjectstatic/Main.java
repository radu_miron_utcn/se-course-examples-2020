package edu.tucn.secourseexamples.lesson3.ex1classobjectstatic;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Main {
    public static void main(String[] args) {
        Person person1 = new Person(1, "Jane", "Doe", "1990-01-01");
        person1.talk();
        person1.walk(4);
        person1.walk(6);
        person1.talk("jfdshgsfd");

        Person person2 = new Person(1, "John", "Doe", "1990-01-01");
        person2.talk();
        person2.walk(10);

        person1.meets(person2);

        // the total distance walked by the two persons
        int totalDistance = person1.getWalkedDistance() + person2.getWalkedDistance();
        System.out.println("Total walked distance: " + totalDistance + " m");

        Person person3 = new Person();

        System.out.println("numOfObjs = " + Person.getCounter());
    }
}
