package edu.tucn.secourseexamples.lesson3.ex1classobjectstatic;

/**
 * @author radumiron
 * @since 18.03.2020
 */

// TODO 1: talk about abstraction
// TODO 2: talk about 'this' and 'this()'
// TODO 3: add 2 static members and 2 static methods:
//                 - counter for all the objects; a method for printing the value
//                 - total walked distance for all the objects; a method for printing the value
// TODO 4: talk about overloading methods and constructors (hint for method: standard walking distance)

public class Person {
    // attributes - they hold the object state
    private static int counter;

    int idNumber;
    String firstName;
    String lastName;
    String dateOfBirth;
    int walkedDistance;

    // constructors - they are used for initializing objects'
    public Person() {
        counter++;
    }

    public Person(String firstName, String lastName) {
        this();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public Person(int idNumber, String firstName, String lastName, String dateOfBirth) {
        this(firstName, lastName);

        this.idNumber = idNumber;
        this.dateOfBirth = dateOfBirth;
    }

    // methods - they define the objects' behaviour
    public void talk() {
        System.out.println(this.firstName + " " + this.lastName + " says something.");
    }

    public void talk(String word) {
        System.out.println(this.firstName + " " + this.lastName + " says: " + word);
    }

    public void walk(int someDistance) {
        this.walkedDistance = this.walkedDistance + someDistance;
        System.out.println(
                String.format("%s %s walked %d m so far", this.firstName, this.lastName, this.walkedDistance));
    }

    public void meets(Person person) {
        System.out.println(
                this.firstName + " meets " + person.firstName
        );
    }

    public int getWalkedDistance() {
        return this.walkedDistance;
    }

    public static int getCounter() {
        return counter;
    }
}
