package edu.tucn.secourseexamples.lesson3.ex4polymorphism;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class BMW320 extends Car {
    @Override
    public void goForward() {
        System.out.println("the BMW moves fast");
    }

    @Override
    public void stop() {
        System.out.println("the BMW stops");
    }
}
