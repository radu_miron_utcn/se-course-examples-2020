package edu.tucn.secourseexamples.lesson3.ex4polymorphism;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class FordGT extends Car {
    @Override
    public void goForward() {
        System.out.println("the FordGT moves faster");
    }

    @Override
    public void stop() {
        System.out.println("the FordGT stops");
    }
}
