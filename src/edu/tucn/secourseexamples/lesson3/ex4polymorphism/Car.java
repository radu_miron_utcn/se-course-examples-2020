package edu.tucn.secourseexamples.lesson3.ex4polymorphism;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public abstract class Car {
    public void start(){
        System.out.println("the car starts");
    }

    public abstract void goForward();

    public abstract void stop();
}
