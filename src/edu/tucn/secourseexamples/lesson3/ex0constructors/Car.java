package edu.tucn.secourseexamples.lesson3.ex0constructors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Car {
    private static int counter;

    private int noOfWheels;
    private String color;

    public Car() {
        counter++;
    }

    public Car(int noOfWheels) {
        this.noOfWheels = noOfWheels;
        counter++;
    }

    public void go() {
        System.out.println("go with " + this.noOfWheels);
    }

    public static int getCounter() {
        return counter;
    }
}
