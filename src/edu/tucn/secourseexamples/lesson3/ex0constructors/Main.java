package edu.tucn.secourseexamples.lesson3.ex0constructors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
//        Car car = new Car(4);
//        Car car1 = new Car(5);
//        Car carClone = car1;
//        Car car2 = new Car();
//
//        car.go();
        System.out.println(Car.getCounter());
    }
}
