package edu.tucn.secourseexamples.lesson3.ex3inheritance.ui;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Main {
    // TODO: Implement the rest of the CRUD operations.
    //       When an employee is deleted the right side of the array should be shifted left and the counter decremented.

    public static void main(String[] args) {
        PersonRepository personRepository = new PersonRepository();
        new Window("Person CRUD", personRepository);
    }
}
