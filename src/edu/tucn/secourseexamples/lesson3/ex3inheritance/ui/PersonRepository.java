package edu.tucn.secourseexamples.lesson3.ex3inheritance.ui;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class PersonRepository {
    private PersonEntity[] personEntities = new PersonEntity[500];
    private int counter = 0;

    public void create(PersonEntity personEntity) {
        personEntities[counter] = personEntity;
        counter++;
    }

    public PersonEntity read(String idNumber) {
        for (PersonEntity personEntity : personEntities) {
            if (idNumber.equals(personEntity.getIdNumber())) {
                return personEntity;
            }
        }

        return null;
    }

    public void update(PersonEntity personEntity) {
    }

    public void delete(String idNumber) {
    }
}
