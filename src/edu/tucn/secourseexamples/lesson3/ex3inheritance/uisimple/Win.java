package edu.tucn.secourseexamples.lesson3.ex3inheritance.uisimple;

import javax.swing.*;
import java.awt.*;
import java.io.FileWriter;
import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private JLabel label;
    private JTextField tf;
    private JButton saveButton;

    public Win() {
        super("My Test");
        this.setLayout(null);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setBounds(20, 20, 200, 200);

        label = new JLabel("Name:");
        label.setBounds(20, 20, 50, 20);

        tf = new JTextField();
        tf.setBounds(80, 20, 90, 20);

        saveButton = new JButton("Save");
        saveButton.setBounds(20, 60, 160, 20);
        saveButton.addActionListener(
                e -> {
                    try (FileWriter fileWriter = new FileWriter("/tmp/se-test.txt", true)) {
                        fileWriter.write(tf.getText() + "\n");
                        fileWriter.flush();
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                }
        );

        this.add(label);
        this.add(tf);
        this.add(saveButton);

        setVisible(true);
    }
}
