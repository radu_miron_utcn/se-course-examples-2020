package edu.tucn.secourseexamples.lesson3.ex3inheritance.simple;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Individual {
    private String firstName;
    private String lastName;
    private String idNumber;

    public Individual(String firstName, String lastName, String idNumber) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.idNumber = idNumber;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }


// TODO: have you read about the Object class?
//    @Override
//    public String toString() {
//        return "Individual{" +
//                "firstName='" + firstName + '\'' +
//                ", lastName='" + lastName + '\'' +
//                ", idNumber='" + idNumber + '\'' +
//                '}';
//    }
}
