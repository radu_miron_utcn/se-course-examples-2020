package edu.tucn.secourseexamples.lesson3.ex3inheritance.simple;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Employer extends Individual {
    public Employer(String firstName, String lastName, String idNumber) {
        super(firstName, lastName, idNumber);
    }
}
