package edu.tucn.secourseexamples.lesson3.ex3inheritance.simple;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Main {
    public static void main(String[] args) {
        Individual individual = new Individual("Arlon", "Brown", "524-78-1431");
        System.out.println(individual.getFirstName());

        Employee employee = new Employee("John", "Jones", "221-17-4131", 1500);
        System.out.println(employee.getIdNumber());
        System.out.println(employee.getFirstName());

        // TODO: talk about type casts

        // implicit type cast
        Individual i1 = new Employee("John", "Jones", "187-95-1421", 1500);

        System.out.println(i1.getFirstName());
//        i1.getSalaryInEUR(); // it can't be accessed because of the reference type

        // explicit type cast
        Employee e1 = (Employee) i1;
        System.out.println(e1.getSalaryInEUR());

//        Employee e2 = (Employee) individual; // this cannot be done
        Employee e3 = new Employee(
                individual.getFirstName(),
                individual.getLastName(),
                individual.getIdNumber(),
                0
        );

        System.out.println(e3);


        Employee empXYZ =
                new Employee("Ion", "Marian", "123", 300);

        System.out.println(empXYZ.getIdNumber());
    }
}
