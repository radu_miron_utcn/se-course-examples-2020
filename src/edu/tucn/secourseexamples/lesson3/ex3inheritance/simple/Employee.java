package edu.tucn.secourseexamples.lesson3.ex3inheritance.simple;


/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Employee extends Individual {
    private float salaryInEUR;

    public Employee(String firstName, String lastName, String idNumber, float salaryInEUR) {
        super(firstName, lastName, idNumber);
        this.salaryInEUR = salaryInEUR;
    }

    public float getSalaryInEUR() {
        return salaryInEUR;
    }

    public void setSalaryInEUR(float salaryInEUR) {
        this.salaryInEUR = salaryInEUR;
    }

    @Override
    public String getFirstName() {
        return super.getFirstName().toUpperCase();
    }

    @Override
    public String getLastName() {
        return super.getLastName().toUpperCase();
    }

// TODO: have you read about the Object class?
    @Override
    public String toString() {
        return "Employee{" +
                "salaryInEUR=" + salaryInEUR +
                '}';
    }
}
