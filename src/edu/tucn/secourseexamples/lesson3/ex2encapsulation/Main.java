package edu.tucn.secourseexamples.lesson3.ex2encapsulation;

/**
 * @author radumiron
 * @since 18.03.2020
 */
public class Main {
    public static void main(String[] args) {
        WalkingPerson p = new WalkingPerson("Gheorghe");

        p.dummyDataField = 34; // NO NO NO!!! dummyDataField should have private access

        p.walkKilometers(0.5f);
        System.out.println("burned calories: " + p.getBurnedCalories());

        p.walkKilometers(0.5f);
        System.out.println("burned calories: " + p.getBurnedCalories());

        p.walkKilometers(10f);
        System.out.println("burned calories: " + p.getBurnedCalories());
    }
}
