package edu.tucn.secourseexamples.lesson3.ex2encapsulation;

/**
 * @author radumiron
 * @since 18.03.2020
 */

// TODO: 1. encapsulation: one should not be able to access the attributes directly;
//       Why? - minimize ripple effects by keeping secrets, reject inappropriate direct access
// TODO: 2. explain access modifiers: private < package < protected < public

public class WalkingPerson {
    int dummyDataField;

    private static final int BURN_RATE = 250;

    private String name;
    private float walkedDistance;
    private int burnedCalories;

    public WalkingPerson(String name) {
        this.name = name;
    }

    void walkKilometers(float kms) {
        this.walkedDistance += kms;
        burnedCalories += BURN_RATE * kms;
    }

    public int getBurnedCalories() {
        return burnedCalories;
    }
}
