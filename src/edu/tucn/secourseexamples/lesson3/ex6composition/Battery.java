package edu.tucn.secourseexamples.lesson3.ex6composition;

/**
 * @author Radu Miron
 * @version 1
 */
public class Battery {
    public int getChargePercentage() {
        return (int) Math.round(Math.random() * 100);
    }
}
