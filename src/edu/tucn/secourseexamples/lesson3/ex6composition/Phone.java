package edu.tucn.secourseexamples.lesson3.ex6composition;

/**
 * @author Radu Miron
 * @version 1
 */
public class Phone {
    private Battery battery;

    public Phone(Battery battery) {
        this.battery = battery;
    }

    public Phone() {
        this.battery = new Battery();
    }

    public void checkBattery() {
        System.out.println(battery.getChargePercentage());
    }
}
