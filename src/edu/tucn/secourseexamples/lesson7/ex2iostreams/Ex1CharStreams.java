package edu.tucn.secourseexamples.lesson7.ex2iostreams;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author radumiron
 * @since 4/16/19
 */
public class Ex1CharStreams {
    private static final String FILE_DIR_RELATIVE_PATH = "testfiles";

    public static void main(String[] args) {
        String sourceFile = FILE_DIR_RELATIVE_PATH + File.separator + "test.txt";
        String destinationFile = FILE_DIR_RELATIVE_PATH + File.separator + "test-copy.txt";
        copyFileTryCatchFinally(sourceFile, destinationFile);

        // of course you could use Files.copy() or Files.move() for a cleaner code
    }

    public static void copyFileTryCatchFinally(String sourceFilePath, String destinationFilePath) {
        FileReader in = null;
        FileWriter out = null;

        try {
            in = new FileReader(sourceFilePath);
            out = new FileWriter(destinationFilePath);

            int c;

            while ((c = in.read()) != -1) { // ‘c’ is the int value of a char
                out.write(c);
                System.out.print(((char) c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
            } catch (IOException ignored) {
            }
        }
    }

    // alternate implementation with 'try-with-resource'
    public void copyFileTryWithResources(String sourceFilePath, String destinationFilePath) {
        try (FileReader in = new FileReader(sourceFilePath);
             FileWriter out = new FileWriter(destinationFilePath)) {
            int c;

            while ((c = in.read()) != -1) { // ‘c’ is the int value of a char
                out.write(c);
                System.out.print(((char) c));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
