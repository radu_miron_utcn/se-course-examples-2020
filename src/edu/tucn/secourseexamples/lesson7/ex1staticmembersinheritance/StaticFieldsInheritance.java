package edu.tucn.secourseexamples.lesson7.ex1staticmembersinheritance;

/**
 * @author Radu Miron
 * @version 1
 */
public class StaticFieldsInheritance {
    public static void main(String[] args) {
        for (int i = 0; i < 10; i++) {
            new Child1();
        }

        System.out.println(Child1.getInstanceCounter());
        System.out.println(Child2.getInstanceCounter());

        // todo: what is wrong???
    }
}

class Parent {
    protected static int instanceCounter; // static fields are often used as instance counters

    public Parent() {
        instanceCounter++;
    }

    public static int getInstanceCounter() {
        return instanceCounter;
    }
}

class Child1 extends Parent {
    public Child1() {
        instanceCounter++;
    }

    public static int getInstanceCounter() {
        return instanceCounter;
    }
}

class Child2 extends Parent {
    public Child2() {
        instanceCounter++;
    }

    public static int getInstanceCounter() {
        return instanceCounter;
    }
}