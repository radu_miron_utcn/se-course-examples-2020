package edu.tucn.secourseexamples.lesson7.ex1staticmembersinheritance;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class StaticMethodsInheritanceFromSuperClass {
    public static void main(String[] args) {
        AnotherParent.doSomething();
        ChildA.doSomething();
        ChildB.doSomething();
        // method doSomething() is clearly inherited from class AnotherParent

        // create 3 random children (A or B)
        AnotherParent[] parents = new AnotherParent[3];

        for (int i = 0; i < 3; i++) {
            if (new Random().nextInt(10) % 2 == 0) {
                parents[i] = new ChildA();
            } else {
                parents[i] = new ChildB();
            }
        }

        // use polymorphism to generically call doSomethingElse()
        AnotherParent randomInstance = parents[new Random().nextInt(3)];
        System.out.println("\nThe instance is of type: " + randomInstance.getClass().getSimpleName());
        randomInstance.doSomethingElse(); //I shouldn't really do this but it compiles!
                                          //Decompile StaticMethodsInheritanceFromSuperClass() to see what really happens.
    }
}

class AnotherParent {
    public static void doSomething() {
        System.out.println("Something");
    }

    public static void doSomethingElse() {
        System.out.println("Parent says something else");
    }
}

class ChildA extends AnotherParent {
//    @Override //why do I get a compilation error? Because in the case of static methods it is called hiding not overriding
    public static void doSomethingElse() {
        System.out.println("ChildA says something else");
    }
}

class ChildB extends AnotherParent {
//    @Override //why do I get a compilation error?
    public static void doSomethingElse() {
        System.out.println("ChildB says something else");
    }
}
