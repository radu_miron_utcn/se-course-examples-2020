package edu.tucn.secourseexamples.lesson7.ex1staticmembersinheritance;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;

/**
 * @author Radu Miron
 * @version 1
 */
public class StaticMethodsFromInterfaces {
    public static void main(String[] args) throws IOException {
        // static methods are not inherited from interfaces - compilation error if you uncomment the next line
//        Implementation.doSomething();
        Contract.doSomethingStatic();

        // default methods are, and they can be overriden
        new Implementation().doSomethingDefault();
    }
}

interface Contract {
    static void doSomethingStatic(){
        System.out.println("Static message");
    }

    default void doSomethingDefault(){
        System.out.println("Default message");
    }
}

class Implementation implements Contract {
}