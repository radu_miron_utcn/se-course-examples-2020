package edu.tucn.secourseexamples.lesson7.ex3serialization;


import edu.tucn.secourseexamples.lesson7.ex3serialization.repo.PersonRepository;
import edu.tucn.secourseexamples.lesson7.ex3serialization.ui.Window;

import java.lang.reflect.InvocationTargetException;

/**
 * @author radumiron
 * @since 01.04.2019
 */
public class Main {
    // todo: don't forget to input a repo implementation as args[0]

    public static void main(String[] args) {
        if (args.length != 1) {
            throw new IllegalArgumentException("Please specify the repository implementation class name");
        }

        PersonRepository personRepository;

        try {
            // we'll use reflexion to instantiate the repo
            // (only if you are interested) for more details read: https://www.oracle.com/technical-resources/articles/java/javareflection.html
            String repoPackage = PersonRepository.class.getPackage().getName();
            String repoClassCanonicalName = repoPackage + "." + args[0];
            personRepository = (PersonRepository) Class.forName(repoClassCanonicalName)
                    .getDeclaredConstructors()[0].newInstance();
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
            throw new IllegalArgumentException("The chosen repository implementation doesn't exist");
        }

        new Window("Person CRUD", personRepository);
    }
}
