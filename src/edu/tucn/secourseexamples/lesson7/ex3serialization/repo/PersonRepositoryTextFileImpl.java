package edu.tucn.secourseexamples.lesson7.ex3serialization.repo;

import edu.tucn.secourseexamples.lesson7.ex3serialization.dto.PersonDTO;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author radumiron
 * @since 4/16/19
 */
// TODO implement the rest of the methods
public class PersonRepositoryTextFileImpl implements PersonRepository {
    // todo: modify this path on your env
    private static final String REPO_DIR_ABSOLUTE_PATH = "/tmp/person-repo-dir";
    private Path repoDir;

    public PersonRepositoryTextFileImpl() {
        try {
            this.repoDir = Files.exists(Paths.get(REPO_DIR_ABSOLUTE_PATH)) ?
                    Paths.get(REPO_DIR_ABSOLUTE_PATH) :
                    Files.createDirectory(Paths.get(REPO_DIR_ABSOLUTE_PATH));
        } catch (IOException e) {
            throw new RuntimeException("Unable to create the repo dir", e);
        }
    }

    @Override
    public void create(PersonDTO personDTO) {
        try (FileWriter fileWriter = new FileWriter(repoDir.resolve(personDTO.getIdNumber()).toFile())) {
            fileWriter.write(personDTO.toString());
        } catch (IOException e) {
            e.printStackTrace(); // you might wanna show this in the UI (checkout JDialog)
        }
    }

    @Override
    public PersonDTO read(String idNumber) {
        return null; // hint use .substring() to deserialize
    }

    @Override
    public void update(PersonDTO personDTO) {

    }

    @Override
    public void delete(String idNumber) {

    }
}
