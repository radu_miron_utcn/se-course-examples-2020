package edu.tucn.secourseexamples.lesson7.ex3serialization.repo;

import edu.tucn.secourseexamples.lesson7.ex3serialization.dto.PersonDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * @author radumiron
 * @since 01.04.2019
 */
public class PersonRepositoryArrayListImpl implements PersonRepository {
    private List<PersonDTO> persons = new ArrayList<>();

    public void create(PersonDTO personDTO) {
        // todo disallow creation if id number already exists
        persons.add(personDTO);
    }

    public PersonDTO read(String idNumber) {
        for (PersonDTO personDTO : persons) {
            if (idNumber.equals(personDTO.getIdNumber())) {
                return personDTO;
            }
        }

        return null;
    }

    public void update(PersonDTO personDTO) {
        // of course we could use a Map instead o a list to find the person to update faster
    }

    public void delete(String idNumber) {
        // of course we could use a Map instead o a list to find the person to delete faster
    }
}
