package edu.tucn.secourseexamples.lesson11.ex1singleton;

import java.util.List;

/**
 * @author Radu Miron
 */
public class Ex2Singleton {
    public static void main(String[] args) {
        FileWriterThreadSafe fileWriterThreadSafe1 = FileWriterThreadSafe.getInstance();
        fileWriterThreadSafe1.setFileName("test1");

        System.out.println("After the first call of getInstance()");
        System.out.println(fileWriterThreadSafe1.getFileName());

        FileWriterThreadSafe fileWriterThreadSafe2 = FileWriterThreadSafe.getInstance();
        fileWriterThreadSafe2.setFileName("test2");

        System.out.println("After the second call of getInstance()");
        System.out.println("fileWriterThreadSafe1.fileName: " + fileWriterThreadSafe1.getFileName());
        System.out.println("fileWriterThreadSafe2.fileName: " + fileWriterThreadSafe2.getFileName());

        System.out.println("fileWriter1 == fileWriter2? " + (fileWriterThreadSafe1 == fileWriterThreadSafe2));
    }
}

class FileWriterThreadSafe {
    private static volatile FileWriterThreadSafe instance;
    private String fileName;

    private FileWriterThreadSafe() {
    }

    public static FileWriterThreadSafe getInstance() {
        synchronized (FileWriterThreadSafe.class) {
            if (instance == null) {
                instance = new FileWriterThreadSafe();
            }
        }

        return instance;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public synchronized void writeLines(List<String> lines){
        //todo: implement
    }
}