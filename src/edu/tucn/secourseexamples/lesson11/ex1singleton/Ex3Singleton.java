package edu.tucn.secourseexamples.lesson11.ex1singleton;

import java.io.*;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.List;

/**
 * @author Radu Miron
 */
public class Ex3Singleton {
    public static void main(String[] args) throws NoSuchMethodException, IllegalAccessException, InvocationTargetException, InstantiationException {
        // get an instance
        FileWriterSerializable fileWriterSerializable = FileWriterSerializable.getInstance();
        fileWriterSerializable.setFileName("test3");
        System.out.println("fileWriterSerializable.fileName: " + fileWriterSerializable.getFileName());

        // serialize that instance
        try (ObjectOutputStream oos =
                     new ObjectOutputStream(new FileOutputStream("fileWriterSerializable.dat"))) {
            oos.writeObject(fileWriterSerializable);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        // deserialize the previous serialized instance
        FileWriterSerializable fileWriterSerializableDeserialized = null;

        try (ObjectInputStream ois = new ObjectInputStream(new FileInputStream("fileWriterSerializable.dat"))) {
            fileWriterSerializableDeserialized = (FileWriterSerializable) ois.readObject();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        System.out.println("fileWriterSerializableDeserialized.fileName: " + fileWriterSerializableDeserialized.getFileName());


        fileWriterSerializableDeserialized.setFileName("test3.1");

        System.out.println("After changing fileWriterSerializableDeserialized.fileName");
        System.out.println("fileWriterSerializable.fileName: " + fileWriterSerializable.getFileName());
        System.out.println("fileWriterSerializableDeserialized.fileName: " + fileWriterSerializableDeserialized.getFileName());

        System.out.println("fileWriterSerializable == fileWriterSerializableDeserialized "
                + (fileWriterSerializable == fileWriterSerializableDeserialized));

        // reflection
        Constructor constructor =
                FileWriterSerializable.class.getDeclaredConstructor();
        constructor.setAccessible(true);

        FileWriterSerializable fileWriterSerializableInstantiatedWithRefection =
                (FileWriterSerializable) constructor.newInstance();

        System.out.println("fileWriterSerializable == fileWriterSerializableWithRefection "
                + (fileWriterSerializable == fileWriterSerializableInstantiatedWithRefection));
    }
}

class FileWriterSerializable implements Serializable {
    private static FileWriterSerializable instance;
    private String fileName;

    private FileWriterSerializable() {
    }

    public static FileWriterSerializable getInstance() {
        if (instance == null) {
            instance = new FileWriterSerializable();
        }

        return instance;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public void writeLines(List<String> lines) {
        //todo: implement
    }
}