package edu.tucn.secourseexamples.lesson11.ex7iterator;

import java.util.Arrays;
import java.util.List;

/**
 * @author Radu Miron
 */
public class IteratorForJavaUtilList {
    public static void main(String[] args) {
        List<String> name = Arrays.asList("John", "Marry", "Egbert");
        java.util.Iterator it = name.iterator();

        while (it.hasNext()) {
            System.out.println(it.next());
        }
    }
}
