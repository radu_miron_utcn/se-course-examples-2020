package edu.tucn.secourseexamples.lesson5.exc54lambda;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    Win(String name) {
        super(name);
        setBounds(3500, 50, 150, 60);
        JButton jButton = new JButton("Click");

        jButton.addActionListener(e -> System.out.println("Message!"));

        add(jButton);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}
