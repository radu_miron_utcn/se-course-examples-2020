package edu.tucn.secourseexamples.lesson5.ex1interface;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class ClickHandler implements ActionListener {
    @Override
    public void actionPerformed(ActionEvent e) {
        new Win();
    }
}
