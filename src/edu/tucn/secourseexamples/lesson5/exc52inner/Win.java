package edu.tucn.secourseexamples.lesson5.exc52inner;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    Win(String name) {
        super(name);
        setBounds(3500, 50, 150, 60);
        JButton jButton = new JButton("Click");

        jButton.addActionListener(new ButtonHandler());

        add(jButton);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }

    class ButtonHandler implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            System.out.println("Message!");
        }
    }
}
