package edu.tucn.secourseexamples.lesson5.ex3interface;

import javax.swing.*;
import java.awt.*;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() throws HeadlessException {
        this.setSize(150, 100);
        this.setLocation(2000 + new Random().nextInt(500), new Random().nextInt(500) + 100);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);

        JButton button = new JButton("Click to close!");
        button.addActionListener(e -> new Win());

        this.add(button);
        this.setVisible(true);
    }
}
