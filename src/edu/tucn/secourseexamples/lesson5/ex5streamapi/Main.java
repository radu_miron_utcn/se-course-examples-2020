package edu.tucn.secourseexamples.lesson5.ex5streamapi;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {

    public static void main(String[] args) {
        Set<String> vowels = new HashSet<>();
        vowels.addAll(Arrays.asList("a", "e"));

        Arrays.asList("e", "a", "b", "etc.").stream()
//                .map(e -> e.length())
                .filter(s -> vowels.contains(s))
                .reduce((a, b) -> a + b)
                .ifPresent(rez -> System.out.println(rez));
    }
}
