package edu.tucn.secourseexamples.lesson5.ex4onesecdelay;

import java.util.Arrays;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Arrays.asList("a", "b", "c", "d", "etc.")
                .forEach(e -> {
                    System.out.println(e);

                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ignored) {
                    }
                });
    }
}
