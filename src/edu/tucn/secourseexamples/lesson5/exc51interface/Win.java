package edu.tucn.secourseexamples.lesson5.exc51interface;

import javax.swing.*;
import java.awt.event.ActionListener;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    Win(String name) {
        super(name);
        setBounds(3500, 50, 150, 60);
        JButton jButton = new JButton("Click");

        ActionListener buttonHandler = new ButtonHandler();
        jButton.addActionListener(buttonHandler);

        add(jButton);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setVisible(true);
    }
}
