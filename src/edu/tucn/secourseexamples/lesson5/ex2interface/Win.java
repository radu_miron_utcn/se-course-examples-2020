package edu.tucn.secourseexamples.lesson5.ex2interface;

import edu.tucn.secourseexamples.lesson5.ex1interface.ClickHandler;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    public Win() throws HeadlessException {
        this.setSize(150, 100);
        this.setLocation(2000 + new Random().nextInt(500), new Random().nextInt(500) + 100);
        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        JButton button = new JButton("Click to close!");
        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Win();
            }
        });
        this.add(button);
        this.setVisible(true);
    }
}
