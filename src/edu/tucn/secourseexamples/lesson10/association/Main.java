package edu.tucn.secourseexamples.lesson10.association;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Win win = new Win();
        Controller controller = new Controller(win);
        controller.start();
    }
}
