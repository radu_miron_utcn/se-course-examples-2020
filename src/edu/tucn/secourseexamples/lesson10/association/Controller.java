package edu.tucn.secourseexamples.lesson10.association;

import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class Controller extends Thread {
    private Win win;

    public Controller(Win win) {
        this.win = win;
    }

    @Override
    public void run() {
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
        }

        int v = new Random().nextInt(10000);
        this.win.updateLabel(v);
    }
}
