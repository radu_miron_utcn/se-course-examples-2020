package edu.tucn.secourseexamples.lesson10.association;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class Win extends JFrame {
    private JLabel label;

    Win() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300, 200);
        setLayout(null);

        label = new JLabel("Text: [ph]");
        label.setBounds(10, 10, 150, 20);

        add(label);
        this.setVisible(true);
    }

    void updateLabel(int val) {
        String text = label.getText();
        text = text.substring(0, text.indexOf(":") + 2) + val;
        label.setText(text);
    }
}
