package edu.tucn.secourseexamples.lesson10.deadlock;

/**
 * @author Radu Miron
 * @version 1
 */
public class DeadThread extends Thread {
    private Object a;
    private Object b;

    public DeadThread(Object a, Object b) {
        this.a = a;
        this.b = b;
    }

    @Override
    public void run() {
        System.out.println(Thread.currentThread().getName() + ": Starting");

        synchronized (a) {
            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {
            }

            synchronized (b) {
                System.out.println(Thread.currentThread().getName() + "The activity");
            }
        }

        System.out.println(Thread.currentThread().getName() + "Done");
    }
}
