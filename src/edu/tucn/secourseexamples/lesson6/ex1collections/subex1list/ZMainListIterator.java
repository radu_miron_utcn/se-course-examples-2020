package edu.tucn.secourseexamples.lesson6.ex1collections.subex1list;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * @author radumiron
 * @since 08.04.2019
 */
public class ZMainListIterator {
    public static void main(String[] args) {
        // todo: remove all Johns from the list

        System.out.println("Iterate with: 1. forEach, 2. iterator");
        int choice = new Scanner(System.in).nextInt();

        List<Person> list = new ArrayList<>();
        list.add(new Person("111", "Mike", "White", "1 Short St"));
        list.add(new Person("123", "John", "Doe", "2 Long St"));
        list.add(new Person("456", "Jane", "Doe", "3 Black St"));
        list.add(new Person("444", "Matt", "Smith", "4 White St"));
        list.add(new Person("789", "John", "Brown", "2 Long St"));

        System.out.println("initial list size: " + list.size());

        switch (choice) {
            case 1:
                for (Person person : list) {
                    if (person.getFirstName().equals("John")) {
                        list.remove(person);
                    }
                }
                break;
            case 2:
                Iterator<Person> iterator = list.iterator();

                while (iterator.hasNext()) {
                    Person person = iterator.next();

                    if (person.getFirstName().equals("John")) {
                        iterator.remove();
                    }
                }

                break;
            default:
                throw new IllegalArgumentException("Wrong choice!");
        }

        System.out.println("after removal list size: " + list.size());
    }
}
