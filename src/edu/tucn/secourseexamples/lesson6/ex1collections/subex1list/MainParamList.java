package edu.tucn.secourseexamples.lesson6.ex1collections.subex1list;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * @author radumiron
 * @since 08.04.2019
 */
public class MainParamList {
    public static void main(String[] args) {
        // list of strings
        List<String> strings = new ArrayList<>();
        strings.add("ccc");
        strings.add("aaa");
        strings.add("bbb");
        String string1 = strings.get(1); // no type cast needed
        System.out.println("The first string in list: " + string1);

        strings.forEach(s -> System.out.println(s));
        System.out.println();

        Collections.sort(strings);

        strings.forEach(s -> System.out.println(s));
        System.out.println();

        Collections.shuffle(strings);
        System.out.println();

        strings.forEach(s -> System.out.println(s));

        List<Integer> integers = new ArrayList<>();
        integers.add(1);
        integers.add(2);
        integers.add(3);

        int sum = 0;
        for (Integer i : integers) {
            sum += i; // no type cast needed
        }
        System.out.println("sum=" + sum);


        List<Person> persons = new ArrayList<>();
        persons.add(new Person("123", "John", "Doe", "12 Abc St, DC"));
        persons.add(new Person("456", "Jane", "Doe", "12 Abc St, DC"));
        Person person1 = persons.get(0); // no type cast needed
        System.out.println(person1);

        Collections.sort(persons);
        Collections.sort(persons, (o1, o2) -> o1.getLastName().compareTo(o2.getLastName()));

        System.out.println("sorted persons list:");
        persons.forEach(p -> System.out.println(p.toString()));

        // TODO: show how to sort this list also; sort by what?? - Comparable and Comparator
    }
}
