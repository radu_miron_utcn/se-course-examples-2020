package edu.tucn.secourseexamples.lesson6.ex1collections.subex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author radumiron
 * @since 09.04.2019
 */
public class Ex3SortedMap {
    public static void main(String[] args) {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("BB", "definition1");
        dictionary.put("Aa", "definition2");
        dictionary.put("T", "definition3");
        dictionary.put("CDE", "definition4");
        dictionary.put("1", "definition5");
        dictionary.put("2", "definition6");

        dictionary.forEach((k,v)-> System.out.println(k));

        //todo: use tree map to sort this map (homework)

//        for (Map.Entry<String, String> kv : dictionary.entrySet()) {
//            System.out.println(kv.getKey() + ": " + kv.getValue());
//        }
    }
}
