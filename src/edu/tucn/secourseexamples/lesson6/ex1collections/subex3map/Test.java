package edu.tucn.secourseexamples.lesson6.ex1collections.subex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author radumiron
 * @since 4/10/19
 */
public class Test {
    public static void main(String[] args) {
        System.out.println("Aa".hashCode());
        System.out.println("BB".hashCode());

        Map<String, String> map = new HashMap<>();
        map.put("BB", "aaaaaaaa");
        map.put("BB", "bbbbbbbb");

        map.forEach((k, v) -> System.out.println(k + ": " + v));
    }
}
