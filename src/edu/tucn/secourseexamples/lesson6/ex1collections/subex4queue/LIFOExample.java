package edu.tucn.secourseexamples.lesson6.ex1collections.subex4queue;

import java.util.Stack;

/**
 * @author radumiron
 * @since 4/10/19
 */
public class LIFOExample {
    public static void main(String[] args) {
        Stack<String> lifo = new Stack<>();
        lifo.push("1");
        lifo.push("1");
        lifo.push("2");
        lifo.push("3");

        int lifoSize = lifo.size();

        for (int i = 0; i < lifoSize; i++) {
            System.out.println(lifo.pop()); // alternatively you can use peek() - what's the difference?
        }
    }
}
