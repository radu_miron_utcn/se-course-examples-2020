package edu.tucn.secourseexamples.lesson6.ex1exceptions;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.UUID;

/**
 * @author radumiron
 * @since 4/16/19
 * <p>
 * We'll try to read a file that doesn't exist
 */
public class Ex1CheckedExceptionExample {
    public static void main(String[] args) {
        String randomUUID = UUID.randomUUID().toString(); // obviously, this file doesn't exist

        try {
            Files.lines(Paths.get(randomUUID)) // <=> Files.readAllLines(Paths.get(randomUUID)).stream()
                .forEach(l -> System.out.println(l));

        } catch (IOException e) {
            // this let you decide what to do if an error occurs
            // for instance we can try reading a file given by the user
            System.err.println(randomUUID + " doesn't exist!"); // or: e.printStackTrace();
            // todo: HOMEWORK - implement the logic for giving the application an existing file; read it and print it's lines
        }
    }
}
