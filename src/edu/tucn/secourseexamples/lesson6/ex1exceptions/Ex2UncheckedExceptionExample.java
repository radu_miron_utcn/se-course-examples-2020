package edu.tucn.secourseexamples.lesson6.ex1exceptions;

/**
 * @author radumiron
 * @since 4/16/19
 */
public class Ex2UncheckedExceptionExample {
    public static void main(String[] args) {
        // NullPointerException
//        Object a = null;
//        System.out.println(a.equals(new Object()));

        // ArrayIndexOutOfBoundsException
    //        List<String> l = Arrays.asList("abc", "def");
    //        System.out.println(l.get(2));

        // ArithmeticException
        int res = 5 / 0;
        System.out.println(res);
    }
}
