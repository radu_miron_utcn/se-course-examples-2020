package edu.tucn.secourseexamples.lesson6.ex1exceptions;

/**
 * @author radumiron
 * @since 4/16/19
 */
public class Ex3Errors {
    public static void a() {
        System.out.println("this is 'a()'");
        b();
    }

    public static void b() {
        System.out.println("this is 'b()'");
        a();
    }

    public static void c() {
        System.out.println("this is 'c()'");
        c();
    }

    public static void main(String[] args) {
//        a(); // or call c();
        c();
    }
}
