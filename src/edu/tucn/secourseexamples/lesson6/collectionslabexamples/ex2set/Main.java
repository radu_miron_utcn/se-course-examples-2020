package edu.tucn.secourseexamples.lesson6.collectionslabexamples.ex2set;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    private static final String MIRCEA = "Mircea1";

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();
        set.add(MIRCEA);
        set.add(MIRCEA);

        set.forEach(System.out::println);
    }
}
