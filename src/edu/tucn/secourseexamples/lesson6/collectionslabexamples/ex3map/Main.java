package edu.tucn.secourseexamples.lesson6.collectionslabexamples.ex3map;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Map<String, String> dictionary = new HashMap<>();
        dictionary.put("cat", "pisica");
        dictionary.put("dog", "caine");
        dictionary.put("bird", "pasare");
        dictionary.put("girl", "fata");
        dictionary.put("boy", "baiat");
        dictionary.put("cat", "pisica updatata");

        System.out.println(dictionary.get("cat"));
        System.out.println("cat".hashCode());

        System.out.println("Aa".hashCode());
        System.out.println("BB".hashCode());
        System.out.println(Integer.MAX_VALUE);

        dictionary.put("Aa", "test1");
        dictionary.put("BB", "test2");
        dictionary.put("BB", "test3");

        System.out.println(dictionary.size());
        System.out.println(dictionary.get("Aa"));
        System.out.println(dictionary.get("BB"));

//        dictionary.forEach((k, v) ->
//                System.out.println(String.format("%s = %s", k, v)));
    }
}