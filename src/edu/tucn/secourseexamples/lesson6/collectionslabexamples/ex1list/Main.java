package edu.tucn.secourseexamples.lesson6.collectionslabexamples.ex1list;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        // arrays
        int[] array = new int[5]; // the array size (length) is fixed

        // vs. lists
        List list = new ArrayList();
        list.add(2);
        list.add("Ion");

        Integer i1 = (Integer) list.get(0);

        // not recommended
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        System.out.println("----------------------------------");

        // almost there
        for (Object el : list) {
            System.out.println(el);
        }
        System.out.println("----------------------------------");

        // quite there
        list.forEach(el -> System.out.println(el));
        System.out.println("----------------------------------");

        // yep we're there
        list.forEach(System.out::println);
        System.out.println("----------------------------------");

        // generics
        List<String> paramedList = new ArrayList<>();
        paramedList.add("Mircea");
        paramedList.add("Mircea");
        String s1 = paramedList.get(0);
        System.out.println(s1);
        paramedList.forEach(System.out::println);
    }
}
