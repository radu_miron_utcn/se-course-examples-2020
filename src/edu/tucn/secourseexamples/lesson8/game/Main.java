package edu.tucn.secourseexamples.lesson8.game;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        Character character = new Character();
        character.setSize(Utils.CHARACTER_SIZE, Utils.CHARACTER_SIZE);

        List<Enemy> enemies = new ArrayList<>();
        for (int i = 0; i < 5; i++) {
            Enemy enemy = new Enemy();
            enemy.setSize(Utils.CHARACTER_SIZE, Utils.CHARACTER_SIZE);
            enemies.add(enemy);
        }

        new MainWindow(character, enemies);

        new Controller(enemies).start();
    }
}
