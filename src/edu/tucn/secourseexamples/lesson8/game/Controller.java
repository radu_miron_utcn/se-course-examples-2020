package edu.tucn.secourseexamples.lesson8.game;

import java.util.List;

/**
 * @author Radu Miron
 * @version 1
 */
public class Controller {
    private List<Enemy> enemies;
    private boolean collision;

    public Controller(List<Enemy> enemies) {
        this.enemies = enemies;
    }

    public void start() {
        while (!collision) {
            enemies.forEach(e -> {
                e.setLocation(e.getX(), e.getY() + 5);
                // todo calculate collision
            });

            try {
                Thread.sleep(200);
            } catch (InterruptedException ignored) {
            }
        }
    }
}
