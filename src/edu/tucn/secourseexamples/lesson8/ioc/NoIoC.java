package edu.tucn.secourseexamples.lesson8.ioc;

/**
 * @author Radu Miron
 * @version 1
 */
public class NoIoC {
    public static void main(String[] args) {
        met1();
        met2();
        // ...
    }

    static void met1(){
        System.out.println("abc");
    }

    static void met2(){
        System.out.println("efg");
    }
}
