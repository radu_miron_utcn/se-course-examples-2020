package edu.tucn.secourseexamples.lesson8.ioc;

import javax.swing.*;

/**
 * @author Radu Miron
 * @version 1
 */
public class IoC {
    public static void main(String[] args) {
        JFrame jFrame = new JFrame("IoC");

        JButton button = new JButton("Click!");

        button.addActionListener(e -> {
            NoIoC.met1();
            NoIoC.met2();
        });

        jFrame.add(button);
        jFrame.setSize(150, 50);
        jFrame.setVisible(true);
    }
}
