package edu.tucn.secourseexamples.lesson2.ex1;

/**
 * @author Radu Miron
 */
public class Main {
    public static void main(String[] args) {
        Cat batman = new Cat("Batman", "black", 25);
        Cat batgirl = new Cat("Batgirl", "pink", 18);

        batman.meow();
        batgirl.meow();

        batgirl.fight(batman);
        batman.fight(batgirl);
    }
}
