package edu.tucn.secourseexamples.lesson2.ex1;

/**
 * @author Radu Miron
 */
public class Cat {
    private String name;
    private String color;
    private int age;

    public Cat(String name, String color, int age) {
        this.name = name;
        this.color = color;
        this.age = age;
    }

    public void meow() {
        System.out.println(this.name + " meows");
    }

    public void fight(Cat anyCat) {
        System.out.println(this.name + " fights " + anyCat.name);
    }
}
