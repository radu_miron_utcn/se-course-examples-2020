package edu.tucn.secourseexamples.lesson9.filemanagers;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileReader {
    public static void main(String[] args) throws IOException {
        String path = "/tmp/test.txt";
        System.out.println(String.format("Char 'a' appears %d times in file %s", processFile(path), path));
    }

    public static int processFile(String path) throws IOException {
        System.out.println("Processing on thread: " + Thread.currentThread().getName());
        long t1 = System.currentTimeMillis();
        int[] counter = {0};
        Path filePath = Paths.get(path);
        Files.lines(filePath)
                .forEach(l -> counter[0] += l.chars().filter(c -> c == 'a').count());
        long t2 = System.currentTimeMillis();
        System.out.println(String.format("The file reading took %f sec", (t2 - t1) / 1000d));
        return counter[0];
    }
}
