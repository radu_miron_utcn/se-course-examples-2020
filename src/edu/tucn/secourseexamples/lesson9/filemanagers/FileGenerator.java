package edu.tucn.secourseexamples.lesson9.filemanagers;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

/**
 * @author Radu Miron
 * @version 1
 */
public class FileGenerator {
    public static void main(String[] args) {
        File file = new File("/tmp/test2.txt");

        if (file.exists()) {
            file.delete();
        }

        StringBuilder lineBuilder = new StringBuilder();
        try (FileWriter fileWriter = new FileWriter(file)) {
            for (int i = 0; i < 5000000; i++) {
                lineBuilder.setLength(0);

                for (int j = 0; j < 80; j++) {
                    lineBuilder.append((char) new Random().nextInt(255));
                }

                lineBuilder.append("\n");

                fileWriter.append(lineBuilder.toString());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        System.out.println(file.length() / (1024 * 1024) + " MB");
    }
}
