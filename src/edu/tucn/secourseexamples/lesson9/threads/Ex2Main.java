package edu.tucn.secourseexamples.lesson9.threads;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex2Main {
    public static void main(String[] args) throws InterruptedException {
        Thread.currentThread().setName("Principal");

        new Thread(() -> {
            try {
                writeMessages(10);
            } catch (InterruptedException ignored) {
            }
        }).start();

        Runnable myRunnable = () -> {
            try {
                writeMessages(10);
            } catch (InterruptedException ignored) {
            }
        };

        new Thread(myRunnable).start();

        writeMessages(10);
    }

    public static void writeMessages(int n) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("%s: message %d", Thread.currentThread().getName(), i));
            Thread.sleep(100);
        }
    }
}
