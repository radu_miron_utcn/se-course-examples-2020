package edu.tucn.secourseexamples.lesson9.threads;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex1Main {
    public static void main(String[] args) throws InterruptedException {
        Thread.currentThread().setName("Principal");

        Thread t1 = new Thread() {
            @Override
            public void run() {
                try {
                    writeMessages(10);
                } catch (InterruptedException ignored) {
                }
            }
        };
        t1.setName("Ioan");
        t1.start();

        Thread t2 = new Thread() {
            @Override
            public void run() {
                try {
                    writeMessages(10);
                } catch (InterruptedException ignored) {
                }
            }
        };
        t2.setName("Gheorghe");
        t2.start();

        writeMessages(10);
    }

    public static void writeMessages(int n) throws InterruptedException {
        for (int i = 0; i < n; i++) {
            System.out.println(String.format("%s: message %d", Thread.currentThread().getName(), i));
            Thread.sleep(100);
        }
    }
}
