package edu.tucn.secourseexamples.lesson9.threads;

/**
 * @author Radu Miron
 * @version 1
 */
public class Ex3Main {
    private static final int ONE_HOUR_IN_MS = 1000 * 60 * 60 * 24;

    public static void main(String[] args) {
        // slow execution thread
        Thread slowThread = new Thread(() -> {
            Thread.currentThread().setName("SlowExecThread");
            try {
                System.out.println(Thread.currentThread().getName() + " does something");
                System.out.println(Thread.currentThread().getName() + " now waits for 1 day");
                Thread.sleep(ONE_HOUR_IN_MS);
            } catch (InterruptedException ignored) {
                System.err.println(Thread.currentThread().getName() + "was interrupted");
            }
        });
        slowThread.start();

        // interrupter
        new Thread(() -> {
            Thread.currentThread().setName("Interrupter");
            try {
                System.out.println(Thread.currentThread().getName() + " waits 3 sec");
                Thread.sleep(3 * 1000);
                System.out.println(Thread.currentThread().getName() + " will interrupt the other thread");
                slowThread.interrupt();
            } catch (InterruptedException ignored) {
            }
        }).start();
    }
}
