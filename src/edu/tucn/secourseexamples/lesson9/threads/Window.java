package edu.tucn.secourseexamples.lesson9.threads;

import edu.tucn.secourseexamples.lesson9.filemanagers.FileReader;

import javax.swing.*;
import java.io.IOException;

/**
 * @author Radu Miron
 * @version 1
 */
public class Window extends JFrame {
    public Window() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(500, 500);
        setLayout(null);

        JTextField tf = new JTextField();
        tf.setBounds(10, 10, 200, 20);

        JTextArea ta = new JTextArea();
        ta.setBounds(10, 40, 300, 300);

        JButton b = new JButton("Count 'a'");
        b.setBounds(10, 350, 200, 20);
        b.addActionListener(e -> {
            new Thread(()-> {
                String filePath = tf.getText();

                try {
                    int count = FileReader.processFile(filePath);
                    ta.append(String.format("File %s contains %d 'a' characters\n", filePath, count));
                } catch (Exception ex) {
                    JOptionPane.showMessageDialog(
                            Window.this, "An err has occurred: " + ex.getClass() + ": " + ex.getMessage());
                }
            }).start();
        });

        add(tf);
        add(ta);
        add(b);
        setVisible(true);
    }

    public static void main(String[] args) {
        new Window();
    }
}
